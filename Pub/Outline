Efficient Point Merge using Data Parallel Primitives in VTK-m

1. Introduction

Describe point merge and the cases it might be necessary.
Describe VTK-m, and it's DPP approach.
Can this help with mesh coarsning?

2. Background

Talk about the external faces paper.         
Talk about the triangle soup paper.          
Talk about history based topology paper.     
Talk about VTK-m itself with various papers. 

3. Implementation
- Describe the approach for picking up the cell spacing in uniform grid approach
- Describe the other algorithms, the feature is being tested against.
  - Point locator in VTK-m, in multiple iterations.
  - MergePoint and point locator in VTK.

4. Results
Compare against iso surface which merges inherently with, 
triangle soup ammending with point merge(which is faster?)

- Performance tests for the following things.
  1. 1. Triangle soup (3 datasets)
        - Fusion
        - Fishtank
        - Supernova
     2. Randomly generated version (2 datasets)
        - Dense points
        - Sparse points
     Total 5
  2. Apporaches
     - Point Locator (Serial/TBB/CUDA) 3
     - Virtual Grid (Serial/TBB/CUDA) 3
     - VTK methods (either MergePoint or point locator, choose suitable)
     Total 9
  3. Choice of delta
     - Choose multiple (3?)
     Total 3
  Total tests should come out to be (5*9*3 ~ 115)
  
  Study should show the algorithms scale well and are efficient compared to VTK  

5. Conclusion

https://mirror.hmc.edu/ctan/macros/latex/contrib/algorithms/algorithms.pdf

Identified related work

******************* General Duplicate Element search and elemination

@inproceedings{miller2014finely,
  title={Finely-threaded history-based topology computation},
  author={Miller, Robert and Moreland, Kenneth and Ma, Kwan-Liu},
  booktitle={Proceedings of the 14th Eurographics Symposium on Parallel Graphics and Visualization},
  pages={41--48},
  year={2014},
  organization={Eurographics Association}
}

@inproceedings{shin2004efficient,
  title={Efficient topology construction from triangle soup},
  author={Shin, Hayong and Park, Joon C and Choi, Byoung Kyu and Chung, Yun C and Rhee, Siyoul},
  booktitle={Geometric Modeling and Processing, 2004. Proceedings},
  pages={359--364},
  year={2004},
  organization={IEEE}
}

@inproceedings{lessley2017techniques,
  title={Techniques for data-parallel searching for duplicate elements},
  author={Lessley, Brenton and Moreland, Kenneth and Larsen, Matthew and Childs, Hank},
  booktitle={Large Data Analysis and Visualization (LDAV), 2017 IEEE 7th Symposium on},
  pages={1--5},
  year={2017},
  organization={IEEE}
}

******************* Mesh Simplification/Coarsening

@inproceedings{decoro2007real,
  title={Real-time mesh simplification using the GPU},
  author={DeCoro, Christopher and Tatarchuk, Natalya},
  booktitle={Proceedings of the 2007 symposium on Interactive 3D graphics and games},
  pages={161--166},
  year={2007},
  organization={ACM}
}

@techreport{potter2011anisotropic,
  title={Anisotropic mesh coarsening and refinement on GPU architecture},
  author={Potter, Mathew and Gorman, Gerard and Kelly, Paul HJ},
  year={2011},
  institution={tech. report, Imperial College London}
}

******************* Mesh Spacing Heuristic

@inproceedings{kalojanov2011two,
  title={Two-level grids for ray tracing on GPUs},
  author={Kalojanov, Javor and Billeter, Markus and Slusallek, Philipp},
  booktitle={Computer Graphics Forum},
  volume={30},
  number={2},
  pages={307--314},
  year={2011},
  organization={Wiley Online Library}
}

******************* Identify VTK-m papers to refer (5 papers)

@ARTICLE{7466740,
author={K. Moreland and C. Sewell and W. Usher and L. Lo and J. Meredith and D. Pugmire and J. Kress and H. Schroots and K. Ma and H. Childs and M. Larsen and C. Chen and R. Maynard and B. Geveci},
journal={IEEE Computer Graphics and Applications},
title={VTK-m: Accelerating the Visualization Toolkit for Massively Threaded Architectures},
year={2016},
volume={36},
number={3},
pages={48-58},
oi={10.1109/MCG.2016.48},
ISSN={0272-1716},
month={May}}

@inproceedings{lessley2016external,
  title={External facelist calculation with data-parallel primitives},
  author={Lessley, Brenton and Binyahib, Roba and Maynard, Robert and Childs, Hank},
  booktitle={Proceedings of the 16th Eurographics Symposium on Parallel Graphics and Visualization},
  pages={11--20},
  year={2016},
  organization={Eurographics Association}
}

@inproceedings{Li:2017:PIS:3144769.3144773,
 author = {Li, Shaomeng and Larsen, Matthew and Clyne, John and Childs, Hank},
 title = {Performance Impacts of In Situ Wavelet Compression on Scientific Simulations},
 booktitle = {Proceedings of the In Situ Infrastructures on Enabling Extreme-Scale Analysis and Visualization},
 series = {ISAV'17},
 year = {2017},
 isbn = {978-1-4503-5139-3},
 location = {Denver, CO, USA},
 pages = {37--41},
 numpages = {5},
 url = {http://doi.acm.org/355.1145/3144769.3144773},
 doi = {10.1145/3144769.3144773},
 acmid = {3144773},
 publisher = {ACM},
 address = {New York, NY, USA},
}

@inproceedings {pgv.20181094,
booktitle = {Eurographics Symposium on Parallel Graphics and Visualization},
editor = {Hank Childs and Fernando Cucchietti},
title = {{Performance-Portable Particle Advection with VTK-m}},
author = {Pugmire, David and Yenpure, Abhishek and Kim, Mark and Kress, James and Maynard, Robert and Childs, Hank and Hentschel, Bernd},
year = {2018},
publisher = {The Eurographics Association},
ISSN = {1727-348X},
ISBN = {978-3-03868-054-3},
DOI = {10.2312/pgv.20181094}
}

@inproceedings{lessley2017maximal,
  title={Maximal clique enumeration with data-parallel primitives},
  author={Lessley, Brenton and Perciano, Talita and Mathai, Manish and Childs, Hank and Bethel, E Wes},
  booktitle={Large Data Analysis and Visualization (LDAV), 2017 IEEE 7th Symposium on},
  pages={16--25},
  year={2017},
  organization={IEEE}
}
