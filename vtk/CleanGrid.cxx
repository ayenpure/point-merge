#include <string>

#include <vtkCellArray.h>
#include <vtkCleanPolyData.h>
#include <vtkGeometryFilter.h>
#include <vtkMergePoints.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkTimerLog.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGridWriter.h>

int main(int argc, char** argv)
{
  if(argc < 3)
  {
    std::cout << "Incorrect number of arguemnts" << std::endl;
    std::cout << "Usage : cleangrid <tolerance> <dataset> " << std::endl;
    exit(EXIT_FAILURE);
  }
  double tolerance = atof(argv[1]);
  std::string dataset(argv[2]);

  // Run the marching cubes filter here, over provided dataset
  vtkSmartPointer<vtkUnstructuredGridReader> inputReader
    = vtkSmartPointer<vtkUnstructuredGridReader>::New();
  inputReader->SetFileName(dataset.c_str());
  inputReader->Update();

  vtkSmartPointer<vtkUnstructuredGrid> intermDataset = inputReader->GetOutput();

  vtkSmartPointer<vtkGeometryFilter> geometryFilter =
      vtkSmartPointer<vtkGeometryFilter>::New();
  geometryFilter->SetInputData(intermDataset);
  geometryFilter->Update();

  vtkSmartPointer<vtkTimerLog> timer = vtkSmartPointer<vtkTimerLog>::New();
  timer->StartTimer();

  vtkSmartPointer<vtkPolyData> polydata = geometryFilter->GetOutput();
  vtkSmartPointer<vtkPoints> intermPoints = polydata->GetPoints();
  vtkSmartPointer<vtkCellArray> intermCells = polydata->GetPolys();

  // Get basic data needed for performing the merge.
  int numCells, numPoints;
  numCells = intermDataset->GetNumberOfCells();
  if(numCells == 0)
  {
    std::cout << "Number of cells is 0, something went wrong" << std::endl;
    exit(EXIT_FAILURE);
  }
  numPoints = intermPoints->GetNumberOfPoints();

  std::cout << "Number of cells to process " << numCells << std::endl;
  std::cout << "Number of points to process " << numPoints << std::endl;

  vtkSmartPointer<vtkCleanPolyData> cleanPolyData
    = vtkSmartPointer<vtkCleanPolyData>::New();
  cleanPolyData->PointMergingOn();
  cleanPolyData->ToleranceIsAbsoluteOn();
  cleanPolyData->SetAbsoluteTolerance(tolerance);
  cleanPolyData->SetInputData(polydata);
  //cleanPolyData->ConvertLinesToPointsOff();
  //cleanPolyData->ConvertPolysToLinesOff();
  //cleanPolyData->ConvertStripsToPolysOff();
  cleanPolyData->Update();

  std::cout << "Cleaned polydata has "
            << cleanPolyData->GetOutput()->GetNumberOfPoints()
            << " points." << std::endl;
  std::cout << "Cleaned polydata has "
            << cleanPolyData->GetOutput()->GetNumberOfCells()
            << " cells." << std::endl;

  timer->StopTimer();
  std::cout << "Time to merge points : "
            << timer->GetElapsedTime() << std::endl;
}
