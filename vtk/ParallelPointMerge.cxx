#include <string>
#include <vector>

#include <vtkCellArray.h>
#include <vtkSMPMergePoints.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkTimerLog.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGridWriter.h>

#include "tbb/tbb.h"

using namespace tbb;

struct Transport
{
  vtkSmartPointer<vtkSMPMergePoints> locator;
  vtkSmartPointer<vtkIdList> idList;

  Transport(vtkSmartPointer<vtkSMPMergePoints> locator_,
            vtkSmartPointer<vtkIdList> idList_)
  : locator(locator_)
  , idList(idList_)
  {}
};

// The operator to merge different chunks of points
// using the vtkSMPMergePoints. This returns the instances
// of the locators, so that the locators can be merged.
// This operator parallelizes on the number of desired
// locators.
class MergePoints
{
public:
  MergePoints(std::vector<Transport>& locators_,
              std::vector<vtkIdType>& splits_,
              vtkSmartPointer<vtkPoints> points_,
              vtkSmartPointer<vtkIdList> mapping_)
  : locators(locators_)
  , splits(splits_)
  , points(points_)
  , mapping(mapping_)
  {}

  void operator()(const tbb::blocked_range<size_t>& range) const
  {
    for(size_t l_itr = range.begin(); l_itr < range.end(); ++l_itr)
    {
      double point[3];
      vtkSmartPointer<vtkSMPMergePoints> locator = locators.at(l_itr).locator;
      vtkIdType start = splits.at(l_itr);
      vtkIdType end = splits.at(l_itr + 1);
      for(vtkIdType p_itr = start; p_itr < end; ++p_itr)
      {
        vtkIdType index;
        points->GetPoint(p_itr, point);
        locator->InsertUniquePoint(point, index);
        mapping->SetId(p_itr, index);
        //std::cout << p_itr << " -> " << index << std::endl;
      }
    }
  }

private:
  std::vector<Transport> locators;
  std::vector<vtkIdType> splits;
  vtkSmartPointer<vtkPoints> points;
  vtkSmartPointer<vtkIdList> mapping;
};

// The operator to merge the points cointained in locators
// together. We can merge different buckets at once, so the
// operator parallelizes on buckets for all the locators.
class MergeLocators
{
public:
  MergeLocators(std::vector<Transport>& locators_,
                std::vector<vtkIdType>& buckets_)
  : merger(locators_.begin()->locator)
  , begin((++locators_.begin()))
  , end(locators_.end())
  , buckets(buckets_)
  {
    input = vtkSmartPointer<vtkPointData>::New();
    output = vtkSmartPointer<vtkPointData>::New();
  }

  void operator()(const tbb::blocked_range<vtkIdType>& range) const
  {
    std::vector<Transport>::iterator itr;
    for(itr = begin; itr!= end; itr++)
    {
      vtkIdType startInd = range.begin();
      vtkIdType endInd = range.end();
      vtkSmartPointer<vtkSMPMergePoints> locator = itr->locator;
      vtkSmartPointer<vtkIdList> idList = itr->idList;
      for(vtkIdType index = startInd; index < endInd; index++)
      {
        vtkIdType bucketId = buckets.at(index);
        if(locator->GetNumberOfIdsInBucket(bucketId) > 0)
        {
          merger->Merge(locator, bucketId, output, input, idList);
        }
      }
    }
  }

private:
  vtkSmartPointer<vtkSMPMergePoints> merger;
  const std::vector<Transport>::iterator begin;
  const std::vector<Transport>::iterator end;
  std::vector<vtkIdType> buckets;
  vtkSmartPointer<vtkPointData> input;
  vtkSmartPointer<vtkPointData> output;
};

class FixIndices
{
public:
  FixIndices(std::vector<Transport>& locators_,
             std::vector<vtkIdType> splits_,
             vtkSmartPointer<vtkIdList> mapping_)
  : locators(locators_)
  , splits(splits_)
  , mapping(mapping_)
  {}

  void operator()(tbb::blocked_range<size_t>& range) const
  {
    //Fix mapping
    for(size_t l_itr = range.begin(); l_itr < range.end(); ++l_itr)
    {
      vtkSmartPointer<vtkIdList> localMap = locators.at(l_itr).idList;
      vtkIdType start = splits.at(l_itr);
      vtkIdType end = splits.at(l_itr + 1);
      for(vtkIdType p_itr = start; p_itr < end; ++p_itr)
      {
        // p_itr is index into the bigger array. The bigger array holds
        // indices to the individual idList in the locators after merge.
        vtkIdType localIndex = mapping->GetId(p_itr);
        // The idLists have the final indices into the points array.
        vtkIdType finalIndex = localMap->GetId(localIndex);
        //Update mapping to store the final indices into the points array.
        mapping->SetId(p_itr, finalIndex);
      }
    }
  }

private:
  std::vector<Transport> locators;
  std::vector<vtkIdType> splits;
  vtkSmartPointer<vtkIdList> mapping;
};

int main(int argc, char** argv)
{
  //tbb::task_scheduler_init init(1);

  // Read dataset and get the number of points.
  if(argc < 3)
  {
    std::cout << "Incorrect number of arguemnts" << std::endl;
    std::cout << "Usage : parallelmerge <dataset> <number of locators>"
              << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string dataset(argv[1]);
  vtkIdType numLocators = atoi(argv[2]);
  // numThreads is used to control the number of threads used,
  // and to control chunking the number of buckets to reduce the
  // scheduling overheads.
  int numThreads = tbb::task_scheduler_init::default_num_threads();
  std::cout << "Default TBB threads : " << numThreads << std::endl;
  if(argc == 4)
  {
    numThreads = atoi(argv[3]);
    std::cout << "User set TBB threads : " << numThreads << std::endl;
    //tbb::task_scheduler_init init(numThreads);
  }

  // Run the marching cubes filter here, over provided dataset
  vtkSmartPointer<vtkUnstructuredGridReader> inputReader
    = vtkSmartPointer<vtkUnstructuredGridReader>::New();
  inputReader->SetFileName(dataset.c_str());
  inputReader->Update();

  vtkSmartPointer<vtkTimerLog> timer = vtkSmartPointer<vtkTimerLog>::New();
  timer->StartTimer();

  vtkSmartPointer<vtkUnstructuredGrid> intermDataset = inputReader->GetOutput();
  vtkSmartPointer<vtkPoints> intermPoints = intermDataset->GetPoints();
  vtkSmartPointer<vtkCellArray> intermCells = intermDataset->GetCells();

  // Get basic data needed for performing the merge.
  double bounds[6];
  intermDataset->GetBounds(bounds);
  vtkIdType numCells, numPoints;
  numCells = intermDataset->GetNumberOfCells();
  if(numCells == 0)
  {
    std::cout << "Number of cells is 0, something went wrong" << std::endl;
    exit(EXIT_FAILURE);
  }
  numPoints = intermPoints->GetNumberOfPoints();

  std::cout << "Number of cells to process " << numCells << std::endl;
  std::cout << "Number of points to process " << numPoints << std::endl;

  vtkIdType limit;
  // 1. Create instances of the locator, and use the instances
  //    to merge points.
  //    We do not need to split the original points, just use
  //    indices to access the points from the original array.
  std::vector<Transport> locators;
  locators.reserve(numLocators);
  for(vtkIdType i = 0; i < numLocators; i++)
  {
    vtkSmartPointer<vtkSMPMergePoints> locator
      = vtkSmartPointer<vtkSMPMergePoints>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    locator->Initialize();
    locator->InitPointInsertion(points, bounds);
    vtkSmartPointer<vtkIdList> idList = vtkSmartPointer<vtkIdList>::New();
    locators.emplace_back(locator, idList);
  }

  vtkSmartPointer<vtkIdList> mapping = vtkSmartPointer<vtkIdList>::New();
  mapping->Allocate(numPoints);

  std::vector<vtkIdType> splits;
  vtkIdType start = 0;
  vtkIdType stride = numPoints / numLocators;
  while(start + stride < numPoints)
  {
    splits.push_back(start);
    start += stride;
  }
  splits.push_back(numPoints);

  MergePoints merge_points(locators, splits, intermPoints, mapping);
  limit = splits.size() - 1;
  tbb::parallel_for(tbb::blocked_range<size_t>(0, limit), merge_points);

  // 2. Gather total number of points to be allocated
  //    Populate the skeletal data strucutre for the parallel
  //    merge operator.
  //    Use TBB to merge the individual buckets.
  //    Use buckets Ids as range for spawining TBB threads.
  //    Most of the logic is similar to how `vtkSMPMergePolyDataHelper` does
  vtkIdType numBuckets = locators.at(0).locator->GetNumberOfBuckets();
  std::vector<vtkIdType> nonEmptyBuckets;
  nonEmptyBuckets.reserve(numBuckets);
  std::vector<bool> bucketVisited(numBuckets, false);
  vtkIdType locatorTotal = 0;
  for(const auto& transport : locators)
  {
    vtkSmartPointer<vtkSMPMergePoints> locator = transport.locator;
    vtkSmartPointer<vtkIdList> idList = transport.idList;
    vtkIdType pointsInLocator = locator->GetPoints()->GetNumberOfPoints();
    //std::cout << "Point in locator " << i << " : " << pointsInLocator << std::endl;
    idList->Allocate(pointsInLocator);
    locatorTotal += pointsInLocator;
    //Process buckets to find non empty buckets.
    for (vtkIdType i = 0; i < numBuckets; i++)
    {
      if (locator->GetNumberOfIdsInBucket(i) > 0 && !bucketVisited.at(i))
      {
        nonEmptyBuckets.push_back(i);
        bucketVisited[i] = true;
      }
    }

  }
  locators.at(0).locator->InitializeMerge();
  locators.at(0).locator->GetPoints()->Resize(locatorTotal);

  MergeLocators mergeLocators(locators, nonEmptyBuckets);
  limit = nonEmptyBuckets.size();
  tbb::parallel_for(tbb::blocked_range<vtkIdType>(0, limit, limit / numThreads),
                    mergeLocators);

  locators.at(0).locator->FixSizeOfPointArray();

  // 3. Fix map of Ids for the original points array.
  FixIndices fix_indices(locators, splits, mapping);
  limit = splits.size() - 1;
  // Skip the mapping for the first locator, because that would remain unchanged
  tbb::parallel_for(tbb::blocked_range<size_t>(1, limit), fix_indices);

  timer->StopTimer();
  std::cout << "Time to finish merging points and generate mapping : "
            << timer->GetElapsedTime() << std::endl;
  timer->StartTimer();

  std::cout << "After processing, number of points "
            <<  locators.at(0).locator->GetPoints()->GetNumberOfPoints() << std::endl;

  // 4. Create a new dataset for storing new output
  vtkSmartPointer<vtkUnstructuredGrid> newDataSet
    = vtkSmartPointer<vtkUnstructuredGrid>::New();
  newDataSet->Allocate(numCells);
  // Add points to the new dataset
  newDataSet->SetPoints(locators.at(0).locator->GetPoints());
  // Add cell set to the new dataset
  vtkSmartPointer<vtkCellArray> newCellSet = vtkSmartPointer<vtkCellArray>::New();

  // Iterate over cells and then make new cells using new points.
  // Check to see for each cell's connection if the point exists in the locator
  vtkIdType point_cnt, idx;
  vtkIdType* points;
  for (idx = 0, intermCells->InitTraversal() ;
       intermCells->GetNextCell(point_cnt, points) ; ++idx)
  {
    assert(point_cnt == 3);
    for(int i = 0; i < point_cnt; i++)
    {
      vtkIdType pointIdx = points[i];
      vtkIdType newPointIdx = mapping->GetId(pointIdx);
      points[i] = newPointIdx;
    }
    newDataSet->InsertNextCell(intermDataset->GetCellType(idx), point_cnt, points);
  }

  timer->StopTimer();
  std::cout << "Time to build dataset with merged points : "
            << timer->GetElapsedTime() << std::endl;

  /*vtkSmartPointer<vtkUnstructuredGridWriter> outputWriter
    = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
  outputWriter->SetFileName("output_.vtk");
  outputWriter->SetInputData(newDataSet);
  outputWriter->Write();*/
}
