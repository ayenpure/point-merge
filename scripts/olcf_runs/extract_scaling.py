import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping :" in line \
    or "Time to merge points :" in line:
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk", "locator", "virtual32", "virtual64"]
threads = [1, 10, 20, 40, 80, 160]
index = [0, 1, 2, 3, 4, 5]
datasets = ["fusion", "thermal hydraulics", "supernova"]
points = {"fusion" : 4125540, "thermal hydraulics" : 15686430, "supernova" : 24493224}
colors = ["#e7298a", "#7570b3", "#d95f02", "#1b9e77"]
datasetcolors = dict(zip(datasets, colors))
algocolors = dict(zip(algorithms, colors))
legends = {"vtk" : "VTK", \
           "locator" : "Point Locator", \
           "virtual32" : "Virtual Grid (32)", \
           "virtual64" : "Virtual Grid (64)"}

def GetAlgoPlotStyle(label):
  global algocolors
  style = {}
  for algo in algorithms:
    if algo in label:
      style["stroke"] = algocolors.get(algo)
  return style


# Get all the tbb times for VTK-m algorithms
algotimes = {}
for algorithm in algorithms:
  filename = "%s_nc_tbb" %(algorithm)
  if os.path.isfile(filename) and os.access(filename, os.R_OK):
    data = open(filename, "r")
    times = GetTimingsFromFile(data)
    data.close()
    algotimes[filename] = times

outfile = "scaling.csv"
output = open(outfile, "w")
header = "algorithm,dataset,threads,time,rate\n"
output.write(header);
offset_start = 0;
for algorithm, times in algotimes.iteritems():
  offset = 0
  for dataset in datasets:
    numpoints = points.get(dataset)
    if "vtk" in algorithm: 
      time = numpy.amin(times[offset:offset+5])  
      offset += 5
      output.write("%s,%s,%s,%f,%f\n" %(algorithm, dataset, "1", time, float(numpoints / time)))
    else:
      for n_threads in threads:
        time = numpy.amin(times[offset:offset+5])  
        offset += 5
        output.write("%s,%s,%s,%f,%f\n" %(algorithm, dataset, n_threads, time, float(numpoints / time)))
output.close()

data = pandas.read_csv(outfile)
#print data
scaling = data.pivot_table(values='time', columns=['algorithm', 'dataset'], index='threads')
print scaling

#legend = {}
for dataset in datasets:
  numpoints = points.get(dataset)
  print "Plot for %s" %dataset
  canvas = toyplot.Canvas('3.5in', '2.6in')
  axes = canvas.cartesian(xlabel = 'Number of available CPU cores', \
                          ylabel = 'Processing Rate (*1M points/sec)', \
                         )
  for algorithm in algorithms:
    algokey = "%s_nc_tbb" %(algorithm)
    data = scaling[algokey, dataset]
    data = numpoints / data
    # We want points processes per second
    data = data / 1000000
    label = algorithm
    plotstyle = GetAlgoPlotStyle(label)
    legend = legends.get(algorithm)
    if algorithm is "vtk":
      vtkrate = numpy.array(data)[0]
      axes.hlines(vtkrate, style = {"stroke": "#e7298a", "stroke-dasharray": "2, 2"})
      axes._text_colors = itertools.cycle([plotstyle.get("stroke")])              
      axes.text(4, vtkrate, legend, style={"-toyplot-vertical-align":"bottom", "font-size":"10px"})
      continue
    x = index
    y = numpy.array(data) #numpy.delete(numpy.array(data), 3, 0)
    axes.plot(x, y, style=plotstyle)
    axes._text_colors = itertools.cycle([plotstyle.get("stroke")])              
    axes.text(x[-2], y[-2], legend, \
              style={"text-anchor":"start", \
                     "-toyplot-anchor-shift":"2px", \
                     "font-size":"10px", \
                     "-toyplot-vertical-align":"bottom"})

  #axes.vlines(2, title = 'T1')
  axes.vlines(3, title = 'T2', style = {"stroke-dasharray": "2, 2"})
  axes.y.domain.min = 0
  axes.y.domain.max = 30
  axes.x.ticks.locator = \
    toyplot.locator.Explicit(index, threads)
  toyplot.pdf.render(canvas, "%s_scaling.pdf" %dataset.replace(" ", ""))
