import sys
import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping" in line :
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk", "vcluster", "virtual"]
tols = ["d1", "d10"]
parenvs = ["tbb", "cuda"]
datasets = ["fusion", "thermal", "supernova"]

tests_per_experiment = 5;

algotimes = {}
for algorithm in algorithms:
  times = {}
  for tol in tols:
    toltimes = {}
    for parenv in parenvs:
      filename = "%s_%s_%s" %(algorithm, tol, parenv)
      if os.path.isfile(filename) and os.access(filename, os.R_OK):
        data = open(filename, "r")
        toltimes[parenv] = GetTimingsFromFile(data)
        data.close()
    times[tol] = toltimes
  algotimes[algorithm] = times

outfile = "timings.csv"
output = open(outfile, "w")
header = "algorithm,tolerance,datasets,parenv,time\n"
output.write(header);

for algorithm, toltimes in algotimes.iteritems():
  for tol, partimes in toltimes.iteritems():
    for parenv, times in partimes.iteritems():
      offset = 0
      for dataset in datasets:
        time = numpy.amin(times[offset:offset+5])
        towrite =  "%s,%s,%s,%s,%f" %(algorithm, tol, dataset, parenv, time)
        output.write("%s\n" %towrite)
        offset +=5
output.close()

data = pandas.read_csv(outfile)
forplots = data.pivot_table(values='time', columns=['tolerance', 'algorithm', 'parenv'], index='datasets')
print forplots 
