import sys
import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping :" in line \
    or "Time to merge points :" in line:
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk", "locator", "virtual32", "virtual64"]
parenvs = ["tbb", "cuda"]
datasets = ["fusion", "thermal", "supernova"]
points = {"fusion" : 4125540, "thermal" : 15686430, "supernova" : 24493224}

tests_per_experiment = 5;

algotimes = {}
for algorithm in algorithms:
  times = {}
  for parenv in parenvs:
    filename = "%s_nc_%s" %(algorithm, parenv)
    if os.path.isfile(filename) and os.access(filename, os.R_OK):
      data = open(filename, "r")
      times[parenv] = GetTimingsFromFile(data)
      data.close()
  algotimes[algorithm] = times

outfile = "timings.csv"
output = open(outfile, "w")
header = "algorithm,datasets,parenv,time\n"
output.write(header);

for algorithm, partimes in algotimes.iteritems():
  for parenv, times in partimes.iteritems():
    offset = 0
    for dataset in datasets:
      if algorithm is "vtk" or parenv is "cuda":
        time = numpy.amin(times[offset:offset+5])
        infenv = parenv
        if algorithm is "vtk":
          infenv = "serial"
        towrite =  "%s,%s,%s,%f" %(algorithm, dataset, infenv, time)
        output.write("%s\n" %towrite)
        offset +=5
      else:
        time = numpy.amin(times[offset:offset+5])
        towrite =  "%s,%s,%s,%f" %(algorithm, dataset, "serial", time)
        output.write("%s\n" %towrite)
        time = numpy.amin(times[offset+20:offset+25])
        towrite =  "%s,%s,%s,%f" %(algorithm, dataset, parenv, time)
        output.write("%s\n" %towrite)
        offset +=30
output.close()

data = pandas.read_csv(outfile)
forplots = data.pivot_table(values='time', columns=['datasets','algorithm'], index='parenv')
print forplots 
