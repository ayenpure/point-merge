import sys
import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping :" in line \
    or "Time to merge points :" in line:
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk","locator", "virtual32", "virtual64"]
parenvs = ["","tbb", "cuda"]
datasets = ["fusion", "thermal hydraulics", "supernova"]
points = {"fusion" : 4125540, "thermal hydraulics" : 15686430, "supernova" : 24493224}

tests_per_thread = 5;

algotimes = {}
for algorithm in algorithms:
  times = {}
  for parenv in parenvs:
    filename = "%s_nc_%s" %(algorithm, parenv)
    if os.path.isfile(filename) and os.access(filename, os.R_OK):
      data = open(filename, "r")
      times[parenv] = GetTimingsFromFile(data)
      data.close()
  algotimes[algorithm] = times

outfile = "timings.csv"
output = open(outfile, "w")
header = "algorithm,datasets,parenv,time\n"
output.write(header);

for algorithm, partimes in algotimes.iteritems():
  for parenv, times in partimes.iteritems():
    offset = 0
    for dataset in datasets:
      if parenv is "cuda" or algorithm is "vtk" :
        time = numpy.amin(times[offset:offset+5])
        infenv = parenv
        if algorithm is "vtk":
          infenv = "serial"
        towrite =  "%s,%s,%s,%f" %(algorithm, dataset, infenv, time)
        output.write("%s\n" %towrite)
        offset +=5
      else:
        time = numpy.amin(times[offset:offset+5])
        towrite =  "%s,%s,%s,%f" %(algorithm, dataset, "serial", time)
        output.write("%s\n" %towrite) 
        time = numpy.amin(times[offset+20:offset+25])
        towrite = "%s,%s,%s,%f" %(algorithm, dataset, parenv, time)
        output.write("%s\n" %towrite)
        offset += 30
output.close()

data = pandas.read_csv(outfile)
forplots = data.pivot_table(values='time', columns=['datasets','algorithm'], index='parenv')
print forplots

#data = pandas.read_csv("serial.csv")
#serial = data.pivot_table(values='time', columns=['algorithm', 'dataset'], index='threads')
#print serial

legands = ["VTK","VTK-m Serial","VTK-m TBB","VTK-m CUDA"]

for dataset in datasets:
  canvas = toyplot.Canvas('4in', '2.6in')
  axes = canvas.cartesian(xlabel = 'Processing Rate (*1M points/sec)', margin=(50, 70))
  numpoints = points.get(dataset)
  vtkmtimes = numpy.array(forplots[dataset, 'virtual32'].values)
  vtktimes = numpy.array(forplots[dataset, 'vtk'].values)
  times = [vtktimes[1], vtkmtimes[1], vtkmtimes[2], vtkmtimes[0]]
  timespm = [time / 1000000 for time in times]
  rate = numpy.reciprocal(timespm)
  print rate
  #temp = {'labels' : legands, 'rate' : rate}
  #toPlot = pandas.DataFrame(data=temp)
  axes.bars(rate, along='y')
  # Label the y axis on the make. This is a bit harder than it should be.
  axes.y.ticks.locator = \
    toyplot.locator.Explicit(labels=legands)
  axes.y.ticks.labels.angle = -45
  # It's usually best to make the y-axis 0-based.
  axes.x.domain.min = 0
  toyplot.pdf.render(canvas, "%s_bar.pdf" %(dataset.replace(" ", "")))
  
