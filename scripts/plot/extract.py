import sys

algorithms = ["nearest", "uniform32", "uniform64"]

if len(sys.argv) < 2:
  print "No argument was provided"
  exit(1)

algorithm = sys.argv[1]
if algorithm not in algorithms:
  print "Not a valid input"
  exit(1)

files = [algorithm+"_tbb", algorithm+"+tbb_pinned"]
datasets = ["fusion", "thermal hydraulics", "supernova"]
threads = [6, 12, 24, 36, 48, 96]

nor_times = []
pin_times = []

for data in files:
  datafile = open(data, "r")
  for line in datafile:
    if "Time to finish merging points and generate mapping" in line :
      time = (line.split(":")[1]).strip()
      print time
