import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping" in line :
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["nearest","uniform32", "uniform64"]
threads = [6, 12, 24, 36, 48, 96]
index = [0, 1, 2, 3, 4, 5]
datasets = ["fusion", "thermal hydraulics", "supernova"]
points = {"fusion" : 4125540, "thermal hydraulics" : 15686430, "supernova" : 24493224}
colors = ["#7570b3", "#d95f02", "#1b9e77"]
datasetcolors = dict(zip(datasets, colors))
algocolors = dict(zip(algorithms, colors))
legends = {"nearest" : "Point Locator", \
           "uniform32" : "Virtual Grid (32)", \
           "uniform64" : "Virtual Grid (64)"}

def GetAlgoPlotStyle(label):
  global algocolors
  style = {}
  for algo in algorithms:
    if algo in label:
      style["stroke"] = algocolors.get(algo)
    if "pinned" not in label:
      style["stroke-dasharray"] = "2, 2"
  return style

algotimes = {}
for algorithm in algorithms:
  filenames = [algorithm+"_tbb", algorithm+"_tbb_pin"]
  for filename in filenames:
    if os.path.isfile(filename) and os.access(filename, os.R_OK):
      data = open(filename, "r")
      times = GetTimingsFromFile(data)
      data.close()
      algotimes[filename] = times      

outfile = "scaling.csv"
output = open(outfile, "w")
header = "algorithm,dataset,threads,time,rate\n"
output.write(header);
offset_start = 0;
for algorithm, times in algotimes.iteritems():
  offset = 0
  for dataset in datasets:
    for n_threads in threads:
      time = 0.
      if "pin" in algorithm and n_threads == 96:
        time = numpy.nan
      else: 
        time = numpy.amin(times[offset:offset+5])  
        offset += 5
      numpoints = points.get(dataset)
      output.write("%s,%s,%s,%f,%f\n" %(algorithm, dataset, n_threads, time, float(numpoints / time)))
output.close()

extensions = ["_tbb","_tbb_pin"]
data = pandas.read_csv(outfile)
scaling = data.pivot_table(values='time', columns=['algorithm', 'dataset'], index='threads')
print scaling

data = pandas.read_csv("serial.csv")
serial = data.pivot_table(values='time', columns=['algorithm', 'dataset'], index='threads')
print serial

#legend = {}
for dataset in datasets:
  print "Plot for %s" %dataset
  canvas = toyplot.Canvas('3.5in', '2.6in')
  axes = canvas.cartesian(xlabel = 'Number of available CPU cores', \
                          ylabel = 'Processing Rate (*1M points/sec)', \
                         )
  numpoints = points.get(dataset)
  for algorithm in algorithms:
    serialtime = serial["%s_serial" %(algorithm), dataset]
    serialrate = numpoints / serialtime 
    serialrate = serialrate / 1000000
    print "%s, %s : %f" %(algorithm, dataset, serialrate)
    for extension in extensions:
      algokey = "%s%s" %(algorithm, extension)
      data = scaling[algokey, dataset]
      data = numpoints / data
      # We want points processes per second
      data = data / 1000000
      label = algorithm
      if "pin" in extension:
        label = "%s pinned" %(label)
      x = index
      if "pin" in extension:
        y = numpy.append(numpy.nan, numpy.delete(numpy.array(data), 3, 0))
      else : 
        y = numpy.append(serialrate, numpy.delete(numpy.array(data), 3, 0))
      plotstyle = GetAlgoPlotStyle(label)
      axes.plot(x, y, style=plotstyle)
      axes._text_colors = itertools.cycle([plotstyle.get("stroke")])              
      if "pin" in extension:
        legend = legends.get(algorithm)
        axes.text(x[-2], y[-2], legend, \
                  style={"text-anchor":"start", \
                         "-toyplot-anchor-shift":"2px", \
                         "font-size":"10px", \
                         "-toyplot-vertical-align":"bottom"})
  vtktime = serial['vtk_serial', dataset]
  vtkrate = numpoints / vtktime
  vtkrate = vtkrate / 1000000 
  axes.hlines(vtkrate, style = {"stroke": "#e7298a", "stroke-dasharray": "2, 2"})
  axes._text_colors = itertools.cycle(["#e7298a"])              
  axes.text(4, 0, "VTK", style={"-toyplot-vertical-align":"bottom", "font-size":"10px"})
  axes.vlines(2, title = 'T1')
  axes.vlines(3, title = 'T2', style = {"stroke-dasharray": "2, 2"})
  axes.y.domain.min = 0
  axes.y.domain.max = 25
  axes.x.ticks.locator = \
    toyplot.locator.Explicit(index, numpy.append([1],numpy.delete(threads, 3, 0)))
  toyplot.pdf.render(canvas, "%s_tol.pdf" %dataset.replace(" ", ""))
