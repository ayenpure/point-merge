import sys
import pandas

if len(sys.argv) < 2:
  print "Incorrect arguments were provided"
  print "Usage : python render.py <csv>"
  exit(1)

datafile = sys.argv[1]
data = pandas.read_csv(datafile)
list(data)
