#include <vtkm/Types.h>

#include <vtkm/cont/DataSet.h>

#include <vtkm/filter/VertexClustering.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

int main(int argc, char** argv)
{
  if(argc < 2)
  {
    std::cout << "Incorrect number of arguemnts" << std::endl;
    std::cout << "Usage : pointmerge <dataset> <variable> <iso values>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string datafile(argv[1]);
  // Get user provided dataset
  vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
  // Use this dataset to ammend connectivity
  vtkm::cont::DataSet dataset = reader.ReadDataSet();

  // Run the VertexClustering filter
  vtkm::filter::VertexClustering clusteringFilter;
  vtkm::Id3 subdivisions(10, 10, 16);
  clusteringFilter.SetNumberOfDivisions(subdivisions);
  vtkm::cont::DataSet output;

  output = clusteringFilter.Execute(dataset);
  vtkm::io::writer::VTKDataSetWriter bWriter("clustered.vtk");
  bWriter.WriteDataSet(output);
}

