#include <vtkm/Types.h>

#include <vtkm/cont/DataSet.h>

#include <vtkm/filter/MarchingCubes.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

int main(int argc, char** argv)
{
  if(argc < 3)
  {
    std::cout << "Incorrect number of arguemnts" << std::endl;
    std::cout << "Usage : pointmerge <dataset> <variable> <iso values>" << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string datafile(argv[1]);
  std::string variable(argv[2]);
  std::vector<vtkm::Float64> isoValues;
  for(int i = 3; i < argc; i++)
  {
    isoValues.push_back(static_cast<vtkm::Float64>(atof(argv[i])));
  }
  // Get user provided dataset
  vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
  // Use this dataset to ammend connectivity
  vtkm::cont::DataSet dataset = reader.ReadDataSet();

  // Run the MarchingCubesFilter
  vtkm::filter::MarchingCubes iso_filter;
  iso_filter.SetIsoValues(isoValues);
  iso_filter.SetMergeDuplicatePoints(false);
  iso_filter.SetActiveField(variable.c_str());

  vtkm::cont::DataSet baseline, toProcess;
  toProcess = iso_filter.Execute(dataset);
  std::cout << "Marching cubes with duplicates reports "
            << toProcess.GetCoordinateSystem().GetData().GetNumberOfValues()
            << " points." << std::endl;

  std::string input =
    datafile.substr(0, datafile.length() - 4).append("_input.vtk");

  vtkm::io::writer::VTKDataSetWriter iWriter(input.c_str());
  iWriter.WriteDataSet(toProcess);

  iso_filter.SetMergeDuplicatePoints(true);
  baseline = iso_filter.Execute(dataset);
  std::cout << "Marching cubes without duplicates reports "
            << baseline.GetCoordinateSystem().GetData().GetNumberOfValues()
            << " points." << std::endl;

  std::string bline =
    datafile.substr(0, datafile.length() - 4).append("_bline.vtk");

  vtkm::io::writer::VTKDataSetWriter bWriter(bline.c_str());
  bWriter.WriteDataSet(baseline);
}

