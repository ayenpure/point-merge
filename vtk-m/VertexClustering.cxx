#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <limits>

#ifdef _BUILDING_TBB_
#include <tbb/task_scheduler_init.h>
#endif

#include <vtkm/cont/Timer.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>

#include <vtkm/filter/VertexClustering.h>

#include "Utilities.hxx"

int main(int argc, char** argv)
{
  std::string datafile;
  bool absolute = true;
  bool fastmode = false;
  vtkm::cont::ArrayHandle<Point> points;
  IdHandle globalMap;
  vtkm::FloatDefault delta;
  vtkm::Bounds bounds;

#ifndef ACCURATE
  std::cout << "High accuracy is disabled" << std::endl;
#else
  std::cout << "High accuracy is enabled" << std::endl;
#endif

  utilities::ParseAndPopulate(argc, argv, datafile, delta, absolute, fastmode);

#ifdef _BUILDING_TBB_
  int numThreads = tbb::task_scheduler_init::default_num_threads();
  std::cout << "Default TBB threads : " << numThreads << std::endl;
  if(argc == 6)
    numThreads = atoi(argv[5]);
  std::cout << "User set TBB threads : " << numThreads << std::endl;
    //make sure the task_scheduler_init object is in scope when running sth w/ TBB
  tbb::task_scheduler_init init(numThreads);
#endif

  using DeviceAdapter = VTKM_DEFAULT_DEVICE_ADAPTER_TAG;

  vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
  vtkm::cont::DataSet toProcess = reader.ReadDataSet();
  //Populate the required fields.
  bounds = toProcess.GetCoordinateSystem().GetBounds();
  vtkm::Vec<vtkm::Id, 3> dimensions;
  utilities::GetGridDimensions(bounds, delta, absolute, fastmode, dimensions);

  //Populate the required fields.
  vtkm::cont::Timer timer;
  utilities::ResetTimer(timer);

  // Run the algorithm
  vtkm::filter::VertexClustering vertexClustering;
  vertexClustering.SetNumberOfDivisions(dimensions);
  vtkm::cont::DataSet amendedDataset = vertexClustering.Execute(toProcess);

  std::cout << "Time to finish merging points and generate mapping : "
            << timer.GetElapsedTime() << std::endl;
  utilities::ResetTimer(timer);

  // Print merge stats
  std::cout << "Output cells : "
            << amendedDataset.GetCellSet().GetNumberOfCells() << std::endl;
  std::cout << "Output points : "
            << amendedDataset.GetCoordinateSystem().GetData().GetNumberOfValues()
            << std::endl;

  /*std::cout << "Writing output dataset" << std::endl;
  vtkm::io::writer::VTKDataSetWriter oWriter("output.vtk");
  oWriter.WriteDataSet(amendedDataset);*/
  return 0;
}
