#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#ifdef _BUILDING_TBB_
#include <tbb/task_scheduler_init.h>
#endif

#include <vtkm/Bounds.h>
#include <vtkm/Types.h>
#include <vtkm/Range.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/PointLocatorSelfJoin.h>
#include <vtkm/cont/Timer.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/DispatcherMapField.h>

#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/WorkletReduceByKey.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>

#include "PointMergeWorklets.h"
#include "Utilities.hxx"

using Point = vtkm::Vec<vtkm::FloatDefault, 3>;
using IdHandle = vtkm::cont::ArrayHandle<vtkm::Id>;

template <typename DeviceAdapter>
void PointMergeNearestNeighbor(vtkm::cont::ArrayHandle<Point>& points,
                               vtkm::FloatDefault delta,
                               vtkm::Bounds& bounds,
                               IdHandle& globalMap,
                               DeviceAdapter)
{
  using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

  assert(points.GetNumberOfValues() != 0);

  vtkm::Id3 dims =  {64, 64, 64};
  vtkm::cont::PointLocatorSelfJoin locator(
    // Dims over here may improve performance,
    // but I am leaving it at this.
    // There is a tradeoff, if we have more cells, updating search
    // structures become slower than usual.
    bounds, dims, delta);

  IdHandle neighbors;
  vtkm::cont::ArrayHandle<vtkm::FloatDefault> distances;
  // Local map for points that merge together.
  IdHandle localMap;
  {
    // Populate the local and global map to initial values.
    vtkm::cont::ArrayHandleIndex indices(points.GetNumberOfValues());
    DeviceAlgorithm::Copy(indices, localMap);
    DeviceAlgorithm::Copy(indices, globalMap);
  }

  vtkm::cont::Timer timer;
  timer.Start();
  bool converged = false;
  while(!converged)
  {
    // Update PointLocator for merging points for the current round
    vtkm::cont::CoordinateSystem coords("merge", points);
    locator.SetCoordinates(coords);
    locator.Update();

    std::cout << "Time to update search strcuture : " << timer.GetElapsedTime()
              << std::endl;
    utilities::ResetTimer(timer);

    // Get the neighbor for each of the points.
    pointmerge::GetNearestNeighbor nnWorklet(delta);
    using NearestNeighborDispatcher =
       vtkm::worklet::DispatcherMapField<pointmerge::GetNearestNeighbor>;
    NearestNeighborDispatcher nnDispatch(nnWorklet);
    nnDispatch.SetDevice(DeviceAdapter());
    nnDispatch.Invoke(points, locator, neighbors, distances);

    std::cout << "Time to find nearest neighbors : " << timer.GetElapsedTime()
              << std::endl;
    utilities::ResetTimer(timer);

    // Check if indices and neighbors are the same.
    // If they are, the merge has converged.
    IdHandle comparison;
    comparison.Allocate(1);
    {
      auto comparePortal = comparison.GetPortalControl();
      comparePortal.Set(0, 0);
    }
    using ComparisonDispatcher =
       vtkm::worklet::DispatcherMapField<pointmerge::CompareWorklet>;
    ComparisonDispatcher comparisonDispatcher;
    comparisonDispatcher.SetDevice(DeviceAdapter());
    comparisonDispatcher.Invoke(neighbors, comparison);

    std::cout << "Time to make decision for next iteration : " << timer.GetElapsedTime()
              << std::endl;
    utilities::ResetTimer(timer);
    {
      auto comparePortal = comparison.GetPortalConstControl();
      if(comparePortal.Get(0) == 0)
      {
        converged = true;
        break;
      }
    }
    // Auxiliary arrays to complete reduce by key properly.
    IdHandle updatedMap;
    vtkm::cont::ArrayHandle<Point> mergedPoints;
    vtkm::cont::ArrayHandleIndex pointIndices(points.GetNumberOfValues());
    // The call to reduce by key does 3 things
    // 1. Reduces the points that are neighbors.
    // 2. Updates the global map with the merged neighbors.
    // 3. Reduces the local map for the next round.
    vtkm::worklet::Keys<vtkm::Id> neighborKeys;
    neighborKeys.BuildArrays(neighbors, vtkm::worklet::KeysSortType::Stable);

    vtkm::worklet::DispatcherReduceByKey<pointmerge::PerformMergeForRound> dispatcher;
    dispatcher.SetDevice(DeviceAdapter());
    dispatcher.Invoke(neighborKeys, pointIndices, points, localMap, globalMap, mergedPoints, updatedMap);

    vtkm::cont::ArrayHandleIndex indices(globalMap.GetNumberOfValues());
    vtkm::worklet::DispatcherMapField<pointmerge::FixNeighborhoodWorklet> fixDispatcher;
    fixDispatcher.SetDevice(DeviceAdapter());
    fixDispatcher.Invoke(indices, globalMap);

    std::cout << "Time to merge points and update maps : " << timer.GetElapsedTime()
              << std::endl;
    utilities::ResetTimer(timer);

    // Update points and local map for the next round.
    DeviceAlgorithm::Copy(mergedPoints, points);
    DeviceAlgorithm::Copy(updatedMap, localMap);

    std::cout << "Time to ready data for next iteration : " << timer.GetElapsedTime()
              << std::endl;
    utilities::ResetTimer(timer);
  }
  utilities::ResetTimer(timer);
  utilities::CondenseGlobalMapIndices(globalMap, DeviceAdapter());
  std::cout << "Time to update global map : " << timer.GetElapsedTime()
            << std::endl;
  utilities::ResetTimer(timer);
}

int main(int argc, char** argv)
{
  std::string datafile;
  vtkm::FloatDefault delta;
  vtkm::cont::ArrayHandle<Point> points;
  IdHandle globalMap;
  vtkm::cont::DataSet dataset;
  bool fastmode = false;
  bool absolute = false;

  utilities::ParseAndPopulate(argc, argv, datafile, delta, absolute, fastmode);

#ifdef _BUILDING_TBB_
  int numThreads = tbb::task_scheduler_init::default_num_threads();
  std::cout << "Default TBB threads : " << numThreads << std::endl;
  if(argc == 6)
    numThreads = atoi(argv[5]);
  std::cout << "User set TBB threads : " << numThreads << std::endl;
  //make sure the task_scheduler_init object is in scope when running sth w/ TBB
  tbb::task_scheduler_init init(numThreads);
#endif

  using DeviceAdapter = VTKM_DEFAULT_DEVICE_ADAPTER_TAG;

  vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
  vtkm::cont::DataSet toProcess = reader.ReadDataSet();

  //Populate the required fields.
  vtkm::Bounds bounds = toProcess.GetCoordinateSystem().GetBounds();
  vtkm::cont::ArrayHandleVirtualCoordinates interm = toProcess.GetCoordinateSystem().GetData();
  vtkm::cont::ArrayCopy(interm, points);

  vtkm::cont::Timer timer;
  timer.Start();
  // Run the algorithm
  PointMergeNearestNeighbor(points, delta, bounds, globalMap, DeviceAdapter());

  std::cout << "Time to finish merging points and generate mapping : "
            << timer.GetElapsedTime() << std::endl;
  utilities::ResetTimer(timer);

  //Amend Connectivity
  vtkm::cont::DataSet amendedDataset;
  utilities::AmendConnectivity(toProcess, globalMap, points, amendedDataset, DeviceAdapter());

  std::cout << "Time to build dataset with merged points : "
            << timer.GetElapsedTime() << std::endl;

  // Print merge stats
  std::cout << "Output cells : "
            << amendedDataset.GetCellSet().GetNumberOfCells() << std::endl;
  std::cout << "Output points : "
            << amendedDataset.GetCoordinateSystem().GetData().GetNumberOfValues()
            << std::endl;

  /*std::cout << "Writing output dataset" << std::endl;
  vtkm::io::writer::VTKDataSetWriter oWriter("output.vtk");
  oWriter.WriteDataSet(amendedDataset);*/
  return 0;
}
