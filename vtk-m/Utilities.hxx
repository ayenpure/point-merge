#ifndef utilities_hxx
#define utilities_hxx

#include <cmath>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <random>
#include <string>
#include <thread>
#include <vector>
#include <limits>
#include <typeinfo>

#include <vtkm/Bounds.h>
#include <vtkm/Types.h>
#include <vtkm/Range.h>
#include <vtkm/VectorAnalysis.h>

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/CellSetExplicit.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/StableSortIndices.h>

#include "PointMergeWorklets.h"

using Point = vtkm::Vec<vtkm::FloatDefault, 3>;
using IdHandle = vtkm::cont::ArrayHandle<vtkm::Id>;

namespace utilities
{

void ResetTimer(vtkm::cont::Timer& timer)
{
  timer.Stop();
  timer.Start();
}

template <typename T>
bool IsSafe(T a, T b)
{
  T max = std::numeric_limits<T>::max();
  if (a > max / b) /* `a * b` would overflow */
    return false;
  return true;
}

template <typename T>
bool ProductOverflow(T dimX, T dimY, T dimZ)
{
  bool safe = IsSafe(dimX, dimY);
  if(safe)
  {
    T res = dimX*dimY;
    return !IsSafe(res, dimZ);
  }
  else
    return true;
}

template <typename CellIndex>
void GetGridDimensions(const vtkm::Bounds& bounds,
                       const vtkm::FloatDefault delta,
                       const bool absolute,
                       const bool fastmode,
                       vtkm::Vec<CellIndex, 3>& dimensions)
{
  CellIndex max_cells = std::numeric_limits<CellIndex>::max();
  vtkm::FloatDefault spacing = (delta == 0 || fastmode) ? delta : 2*delta;
  bool overflow = false;
  std::cout << "X Bounds : " << bounds.X.Min  << " to " << bounds.X.Max << ", Length : " << bounds.X.Length() << std::endl;
  std::cout << "Y Bounds : " << bounds.Y.Min  << " to " << bounds.Y.Max << ", Length : " << bounds.Y.Length() << std::endl;
  std::cout << "Z Bounds : " << bounds.Z.Min  << " to " << bounds.Z.Max << ", Length : " << bounds.Z.Length() << std::endl;
  CellIndex dimX, dimY, dimZ;
  if(absolute)
  {
    std::cout << "Using delta as absolute value" << std::endl;
    dimX = static_cast<CellIndex>(vtkm::Floor(bounds.X.Length() / spacing));
    dimY = static_cast<CellIndex>(vtkm::Floor(bounds.Y.Length() / spacing));
    dimZ = static_cast<CellIndex>(vtkm::Floor(bounds.Z.Length() / spacing));
  }
  else
  {
    std::cout << "Using delta as bounding box fraction" << std::endl;
    // Extents of bounding box
    dimX = static_cast<CellIndex>(1. / delta);
    dimY = static_cast<CellIndex>(1. / delta);
    dimZ = static_cast<CellIndex>(1. / delta);
  }
  // Check for overflow conditions and 0 delta
  if(delta == 0 || ProductOverflow(dimX, dimY, dimZ))
    overflow = true;
  if(overflow)
  {
    CellIndex dimension
      = static_cast<CellIndex>(vtkm::Floor(cbrt((vtkm::FloatDefault)max_cells)));
    dimensions = vtkm::make_Vec(dimension, dimension, dimension);
  }
  else
  {
    dimensions = vtkm::make_Vec(dimX, dimY, dimZ);
  }
  std::cout << "Dimensions for virtual grid : " << dimensions << std::endl;
}

vtkm::FloatDefault Distance(const Point& x, const Point& y)
{
  vtkm::FloatDefault distance =  static_cast<vtkm::FloatDefault>(sqrt( pow(x[0] - y[0], 2)
                                                                     + pow(x[1] - y[1], 2)
                                                                     + pow(x[2] - y[2], 2)));
  return distance;
}

void PrintPointArray(vtkm::cont::ArrayHandle<Point>& pointArray, vtkm::Id numValues = 10)
{
  numValues = std::min(numValues, pointArray.GetNumberOfValues());
  auto portal = pointArray.GetPortalConstControl();
  for(int i = 0; i < numValues; i++)
    std::cout << portal.Get(i) << std::endl;
  std::cout << std::endl;
}

template <typename IdType>
void PrintIdArray(vtkm::cont::ArrayHandle<IdType>& idArray, vtkm::Id numValues = 10)
{
  numValues = std::min(numValues, idArray.GetNumberOfValues());
  auto portal = idArray.GetPortalConstControl();
  std::cout << std::fixed;
  for(size_t i = 0; i < numValues; i++)
  {
    std::cout << portal.Get(i) << " ";
    if(i%10 == 0 && i != 0)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}

void PrintBruteForce(vtkm::cont::ArrayHandle<Point>& points)
{
  std::cout << std::fixed;
  std::cout << std::setprecision(5);
  auto portal = points.GetPortalConstControl();
  for(int i = 0; i < points.GetNumberOfValues(); i++)
  {
    for(int j = 0; j < points.GetNumberOfValues(); j++)
    {
      if(i == j)
        std::cout << "X" << "\t";
      else
        std::cout << Distance(portal.Get(i), portal.Get(j)) << "\t";
    }
    std::cout << std::endl;
  }
}

void ParseAndPopulate(int argc,
                      char** argv,
                      std::string& datafile,
                      vtkm::FloatDefault& delta,
                      bool& absolute,
                      bool& fastmode)
{
  if(argc < 3)
  {
    std::cout << "Invalid number of arguments provided" << std::endl;
    std::cout << "Usage : pointmerge <dataset> <delta> <fastcompare>"
              << std::endl;
    exit(EXIT_FAILURE);
  }
  datafile = std::string(argv[1]);
  // This is required in all cases.
  delta = static_cast<vtkm::FloatDefault>(atof(argv[2]));
  if(argc >= 4)
    absolute = (bool)atoi(argv[3]);
  if(argc >= 5)
    fastmode = (bool)atoi(argv[4]);
}

template <typename DeviceAdapter>
void CondenseGlobalMapIndices(IdHandle& globalMap, DeviceAdapter)
{
  using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;
  // Fix global map such that if there are any indices greater than
  // the number of remining points, they're fixed so that we have the
  // maximum index that corresponds to the number of points.
  IdHandle unique, copy;
  DeviceAlgorithm::Copy(globalMap, unique);
  DeviceAlgorithm::Copy(globalMap, copy);
  DeviceAlgorithm::Sort(unique);
  DeviceAlgorithm::Unique(unique);
  DeviceAlgorithm::LowerBounds(unique, copy, globalMap);
}

class PrintClusters : public vtkm::worklet::WorkletReduceByKey
{
public :
  PrintClusters() = default;

  using ControlSignature = void(KeysIn keys,
                                ValuesIn indices);

  using ExecutionSignature = void(WorkIndex, _1, _2);

  template <typename IndexVecType>
  VTKM_EXEC void operator()(const vtkm::Id& workIndex,
                            const vtkm::Id& key,
                            const IndexVecType& indices) const
  {
    vtkm::Id numComponents = indices.GetNumberOfComponents();
    std::cout << "Work Index : " << workIndex << ", " << key << " : ";
    for(vtkm::Id i = 0; i < numComponents; i++)
    {
      std::cout << indices[i] << " ";
    }
    std::cout << std::endl;
  }
};

class GetPointsForCell : public vtkm::worklet::WorkletMapPointToCell
{
public:
  GetPointsForCell(vtkm::Id numPoints)
  : NumPoints(numPoints)
  {}

  using ControlSignature = void(CellSetIn,
                                FieldInPoint,
                                FieldOutCell,
                                FieldOutCell);

  using ExecutionSignature = void(_2, _3, _4);

  template <typename VertexVecType>
  VTKM_EXEC void operator()(const VertexVecType& mapped,
                            vtkm::Id& select,
                            vtkm::Id3& vertices) const
  {
    if(mapped[0] == mapped[1] ||
       mapped[1] == mapped[2] ||
       mapped[2] == mapped[0] )
    {
      select = 0;
      vertices[0] = vertices[1] = vertices[2] = this->NumPoints;
    }
    else
    {
      select = 1;
      vertices[0] = mapped[0];
      vertices[1] = mapped[1];
      vertices[2] = mapped[2];
    }
  }
private:
  vtkm::Id NumPoints;
};

template <typename ValueType, typename StorageType, typename IndexArrayType, typename DeviceAdapter>
VTKM_CONT vtkm::cont::ArrayHandle<ValueType> ConcretePermutationArray(
  const IndexArrayType& indices,
  const vtkm::cont::ArrayHandle<ValueType, StorageType>& values,
  DeviceAdapter)
{
  vtkm::cont::ArrayHandle<ValueType> result;
  auto tmp = vtkm::cont::make_ArrayHandlePermutation(indices, values);
  vtkm::cont::Algorithm::Copy(tmp, result);
  return result;
}

template <typename T, vtkm::IdComponent N, typename DeviceAdapter>
vtkm::cont::ArrayHandle<T> copyFromVec(vtkm::cont::ArrayHandle<vtkm::Vec<T, N>> const& other,
                                       DeviceAdapter)
{
  const T* vmem = reinterpret_cast<const T*>(&*other.GetPortalConstControl().GetIteratorBegin());
  vtkm::cont::ArrayHandle<T> mem =
    vtkm::cont::make_ArrayHandle(vmem, other.GetNumberOfValues() * N);
  vtkm::cont::ArrayHandle<T> result;
  vtkm::cont::ArrayCopy(mem, result, DeviceAdapter());
  return result;
}

template <typename DeviceAdapter>
void AmendConnectivity(vtkm::cont::DataSet& inputDataset,
                       vtkm::cont::ArrayHandle<vtkm::Id>& amendMap,
                       vtkm::cont::ArrayHandle<Point>& points,
                       vtkm::cont::DataSet& outputDataset,
                       DeviceAdapter)
{
  vtkm::cont::DynamicCellSet cellset = inputDataset.GetCellSet();

  if(cellset.IsType<vtkm::cont::CellSetStructured<2>>()
     || cellset.IsType<vtkm::cont::CellSetStructured<3>>())
  {
    outputDataset = inputDataset;
    return;
  }

  vtkm::Id numPoints = points.GetNumberOfValues();
  vtkm::cont::ArrayHandle<vtkm::Id> cellSelect;
  vtkm::cont::ArrayHandle<vtkm::Id3> cellPoints, finalConnectivity;
  GetPointsForCell pointsForCellWorklet(numPoints);
  vtkm::worklet::DispatcherMapTopology<GetPointsForCell> pointsForCellDispatcher(pointsForCellWorklet);
  pointsForCellDispatcher.SetDevice(DeviceAdapter());
  pointsForCellDispatcher.Invoke(cellset, amendMap, cellSelect, cellPoints);

  vtkm::cont::Algorithm::CopyIf(cellPoints, cellSelect, finalConnectivity);
  cellPoints.ReleaseResources();
  cellSelect.ReleaseResources();

  vtkm::cont::CoordinateSystem coords("amended", points);
  outputDataset.AddCoordinateSystem(coords);
  vtkm::cont::CellSetSingleType<> triangles("cells");
  triangles.Fill(points.GetNumberOfValues(),
                 vtkm::CellShapeTagTriangle::Id,
                 3,
                 copyFromVec(finalConnectivity, DeviceAdapter()));
  outputDataset.AddCellSet(triangles);
}

template <typename DeviceAdapter>
void AnalyzeMeshIssues(vtkm::cont::DataSet& dataset, DeviceAdapter)
{
   using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

  vtkm::cont::ArrayHandle<vtkm::Id> markDegenerate;
  vtkm::worklet::DispatcherMapTopology<pointmerge::DegenerateTriangles> degenerate;
  degenerate.SetDevice(DeviceAdapter());
  degenerate.Invoke(dataset.GetCellSet(), markDegenerate);
  vtkm::Id numberDegenerateCells = DeviceAlgorithm::Reduce(markDegenerate, vtkm::Id(0));
  std::cout << "Number of degenerate cells : " << numberDegenerateCells << std::endl;

  vtkm::Id numVertices = dataset.GetCoordinateSystem().GetData().GetNumberOfValues();
  pointmerge::BadVertexIndex badVertexWorklet(numVertices);
  vtkm::worklet::DispatcherMapTopology<pointmerge::BadVertexIndex> badVertex(badVertexWorklet);
  badVertex.SetDevice(DeviceAdapter());
  badVertex.Invoke(dataset.GetCellSet());
}

} //namespace utilities

#endif
